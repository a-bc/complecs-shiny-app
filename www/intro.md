---
title: "info"
output: html_document
---

# Welcome to the COMPLECS app!

When developing a behavior change intervention, it is first necessary to obtain a comprehensive in-depth understanding of the problem. For example, the Intervention Mapping protocol dedicated the first step, the Needs Assessment, to this endeavour. In this phase, maintaining an overview can be challenging, as all the myraid aspects of reality that are, or may be, relevant are collected and related to each other.

COMPLECS^[COMPLECS is a recursive acronym for COMPLECS Organises Multiple Players & Linked Environments using Connected Specifications.] was developed to help to keep an overview during this process. It allows adding new information in a piece-wise manner, and then combines everything into one visualisation.

COMPLECS reads the specifications from a spreadsheet. This spreadsheet can be provided either as an URL to Google Sheets (useful if you want to collaborate with multiple people) or as the path to an Excel file.

## How to use this app

If you click the Data tab at the top, you can either enter the link to a Google Sheet (great if you collaborate on your COMPLECS overview - make sure it's publicly viewable!) or upload a spreadsheet in .xlsx (Excel) format.

## Example COMPLECS

To look at a simple example, you can check out the COMPLECS specification at https://docs.google.com/spreadsheets/d/1WMO15xroy4a0RfpuZ8GhT-NfDoxwS34w9PrWp8rGjjk/edit#gid=931780891 - and copy-paste is into the "URL" field in the "App" tab.

## The COMPLECS specification

The spreadsheet workbook should contain four worksheets. The default names^[These names of the worksheets and their columns can be specified using the `behaviorchange::opts$set()` function.] of these worksheets and columns in those worksheets are as follows.

In the **`entities` worksheet**, you specify the entities you identified. These become the *nodes* in the visualisations. Entities can be people such as a member of the at-risk population, the target population, or another relevant actor; they can be more abstract entities such as school boards, hospital management, or politicians; they can be environmental conditions, such as the presence of free condoms or drug testing services; or they can be behaviors, such as bullying or providing social support. In fact, entities can be anything that you think is useful to include in the overview; there are really no limitations except what works for you. This flexibility is possible because entities have types, which are specified in another worksheet. When specifying an entity, you have to specify a unique identifier (in column `entity_id`), a human-readable label (in `entity_label`), and an entity type (`entity_type_id`).

In the **`connections` worksheet**, you specify how the entities relate to each other. By default, you can use the relationship types specified in West et al. (2019, [doi:10.1038/s41562-019-0561-2](https://doi.org/10.1038/s41562-019-0561-2)), such as `causal_influences_unspecified` for causal effects where it is unknown whether the effects are positive or negative; `causal_influences_positive` for positive causal effects; `causal_influences_negative` for negative causal effects; and `causal_influences_unknown` if it is unknown whether there is a causal effect. When specifying a connection, you have to specify the entity from which the connection originates (`from_entity_id`), the entity to which that entity connects (`to_entity_id`), and the connection type (`connection_type_id`).

In the **`entity_types` worksheet**, you specify the possible entity types. By default, the following types are prespecified: `person`, `environmental_condition`, `behavior`, and `determinant`. You can add more types here by specifying their type identifier (in column `entity_type_id`) and their human-readable label (in `entity_type_label`), as well as how you want it to look in the COMPLECS overview by specifying the stroke color (`color`), the shape (`shape`), the fill color (`fillcolor`), the style (`style`), and the text color (`fontcolor`).

In the **`connection_types` worksheet**, you specify the possible connection types. By default, all connection types listed in West et al. (2019, [doi:10.1038/s41562-019-0561-2](https://doi.org/10.1038/s41562-019-0561-2)) have been prespecified. You can add more connection types here by specifying their connection type identifier (in column `connection_type_id`) and their human-readable label (in `connection_type_label`), as well as how you want the connection to look in teh COMPLECS overview by specifying the stroke color (`color`), the style (`style`), and the direction of the arrow (`dir`).

# Preparing Google Sheets for data export

If you want to use a Google Sheet, make sure you make it public in two ways (it has to be public in *both* ways for the R `googlesheets` package to be able to read the information). To do this, click the Share button at the top-right and clicking "Get link", making sure the settings allow anybody with the link to view the spreadsheet.
